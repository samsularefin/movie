#include <stdio.h>
#include <stdlib.h>

typedef struct {
  int hour;
  int min;
  int sec;
}Time;           // sub-structure time 

typedef struct {
  int day;
  int month;
  int year;
}Date;          // sub-structure date

typedef struct {                                                                  
  unsigned id ;
  char *name;                                                                   
  int active ;                                                                    
  Date *date;  
  Time *time;  // defined the sub-structures
}Movie;        // main structure  


Movie *make_movie(unsigned id);
void free_movie(Movie *mov);
void make_movie_active(Movie *mov);
int is_movie_active(Movie *mov);
void movie_name(Movie *mov,char *name);
int serialize_movie (char *buffer, Movie *mov);
int deserialize_movie(char *buffer, Movie *mov);
Movie *alloc_blank_movie();
