#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "movie.h"
#include "config.h" 

Movie *make_movie(unsigned id) {
  
  Movie*mov;
  if (( mov=(Movie*)malloc(sizeof(Movie))) == NULL){ // malloc gives a memory 
    fprintf(stderr," Failed to allocate movie structure !\n");// stderr = stands for standard error 
    exit(EXIT_FAILURE);
  }  
  if (( mov->time=(Time*)malloc(sizeof(Movie))) == NULL){ 
    fprintf(stderr," Failed to allocate movie structure !\n");
    exit(EXIT_FAILURE);
  } 
  if (( mov->date=(Date*)malloc(sizeof(Date))) == NULL){ 
     fprintf(stderr," Failed to allocate movie structure !\n");
     exit(EXIT_FAILURE);
  }
     
  mov->active = 0;//initially make movie inactive
  mov->id = id;// id value is set based on the function where the value is called with
  mov->time->hour = 2;// assigning values to the time and date pointers
  mov->time->min = 35;
  mov->time->sec = 24;
  mov->date->day = 6;
  mov->date->month = 10;
  mov->date->year = 2010;
  mov->name = NULL;
  
  return mov;
}

void free_movie(Movie*mov) { 
  free(mov->name);
  free(mov->time);      // free all the maaloc functions that were given memory to
  free(mov->date);
  free(mov);
  
}
void make_movie_active(Movie*mov) {
  mov->active = 1;//calling this function makes the movie active
}
int is_movie_active(Movie*mov){//checks for the movie active or not. if it is active return the the value
  return mov->active;
}
void movie_name(Movie *mov, char *name){ //strdup makes a fresh copy of the string (name)
  mov->name = strdup(name);
}
int serialize_movie (char *buffer, Movie *mov)// buffer allocates memory  in a way that it will be available during the function
{
  size_t offset = 0;//offset copies the structure data to correct position
  memcpy(buffer, &mov->id,sizeof(mov->id));// (uses the offset to modify the pointer location to memory,requires a location in memory,number of bytes to copy)
  offset = sizeof(mov->id);
  memcpy(buffer+offset, &mov->active,sizeof(mov->active));
  offset = offset + sizeof(mov->active);
  memcpy(buffer+offset, mov->name, strlen(mov->name)+1);
  offset = offset + strlen(mov->name)+1;
  
  memcpy(buffer+offset, &mov->time->hour,sizeof(mov->time->hour));
  offset = offset + sizeof(mov->time->hour);
  memcpy(buffer+offset, &mov->time->min,sizeof(mov->time->min));
  offset = offset + sizeof(mov->time->min);
  memcpy(buffer+offset, &mov->time->sec,sizeof(mov->time->sec));
  offset = offset + sizeof(mov->time->sec);
  
  memcpy(buffer+offset, &mov->date->day,sizeof(mov->date->day));
  offset = offset + sizeof(mov->date->day);
  memcpy(buffer+offset, &mov->date->month,sizeof(mov->date->month));
  offset = offset + sizeof(mov->date->month);
  memcpy(buffer+offset, &mov->date->year,sizeof(mov->date->year));
  offset = offset + sizeof(mov->date->year);

  
  return offset;
}

int deserialize_movie (char *buffer, Movie *mov) // this the opposite of the serialisation code
  
{
  size_t offset = 0;
  memcpy(&mov->id,buffer, sizeof(mov->id));
  offset = sizeof(mov->id);
  memcpy(&mov->active, buffer+offset, sizeof(mov->active));
  offset = offset + sizeof(mov->active);
  memcpy(mov->name, buffer+offset, strlen(buffer+offset)+1);
  offset = offset + strlen(buffer+offset)+1;

  memcpy(&mov->time->hour, buffer+offset, sizeof(mov->time->hour));
  offset = offset + sizeof(mov->time->hour);
  memcpy(&mov->time->min, buffer+offset, sizeof(mov->time->min));
  offset = offset + sizeof(mov->time->min);
  memcpy(&mov->time->sec, buffer+offset, sizeof(mov->time->sec));
  offset = offset + sizeof(mov->time->sec);
  
  memcpy(&mov->date->day, buffer+offset, sizeof(mov->date->day));
  offset = offset + sizeof(mov->date->day);
  memcpy(&mov->date->month, buffer+offset, sizeof(mov->date->month));
  offset = offset + sizeof(mov->date->month);
  memcpy(&mov->date->year, buffer+offset, sizeof(mov->date->year));
  offset = offset + sizeof(mov->date->year);
  
  
  

  return offset;
}

Movie *alloc_blank_movie()// blank movie data structure created and will be filled in from the date from the buffer 
{
  Movie *mov;

  if ((mov = (Movie *)malloc(sizeof(Movie))) == NULL) {
    fprintf(stderr, "Failed to allocate movie structure!\n");
    exit(EXIT_FAILURE);
  }
  mov->active = 0;
  mov->id = 0;
  if ((mov->name = malloc(MAX_NAME)) == NULL) {
    fprintf(stderr, "Failed to allocate name!\n");
    exit(EXIT_FAILURE);
  }
  
  return mov;
}

