#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "movie.h" // this is a header file within the program 
#include <errno.h>
#define MAXCHARS 256  // defines maximum charecters should be 256
int main ()                 
{
  Movie *mov1;
  int sock;
  struct sockaddr_in servaddr;
  char buffer[MAX_BUFFER];
  char buffer2 [MAXCHARS];  //  2nd character buffer name is buffer2
  char *end = NULL;
  unsigned id = 0;
  errno = 0;
  ssize_t bytes_encoded,bytes_sent;
  
  
  if ( (sock = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
    fprintf(stderr, "Error creating socket.\n");
    exit(EXIT_FAILURE);
  }
  
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family      = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr(HOST_IP);
  servaddr.sin_port        = htons(HOST_PORT);
  
  if (connect(sock, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
    fprintf(stderr, "Error calling connect()\n");
    exit(EXIT_FAILURE);
  }
  // fgets gets the parameters
  printf ("Enter movie no: "); //user input for the movie no 
  if (fgets(buffer2,MAXCHARS,stdin) == NULL){
    fprintf (stderr, "failed to read string\n");
    return (EXIT_FAILURE);
  }
  // strtok remove the newline charecter and strtok converts the string to an integer
  strtok(buffer2, "\n");
  id = strtoul(buffer2, &end, 10);
  if (errno != 0){
    fprintf(stderr, "conversion error, %s\n",strerror(errno));
    exit (EXIT_FAILURE);
  }
  else if (*end){
    printf("Warning: converted partially:%i,non-convertible part: %s\n",id,end);
  }
 
  printf("Enter movie name: ");//user input for the movie name
  if(fgets(buffer2,MAXCHARS,stdin)== NULL){
    fprintf(stderr, "failed to read string\n");
    return(EXIT_FAILURE);
  }
  strtok(buffer2,"\n");
  mov1 = make_movie(id);
  movie_name(mov1,buffer2);
  make_movie_active(mov1);// calls for the function
  if (is_movie_active(mov1)) {
    printf("movie no %u   the name is %s\n",mov1->id,mov1->name);// after the user input this line would be shown
    printf("movie duration is %d-%d-%d\n", mov1->time->hour,mov1->time->min,mov1->time->sec);// displays the duration of the movie
    printf("movie release date is %d/%d/%d\n",mov1->date->day,mov1->date->month,mov1->date->year);// displays the movie release date
  }  
  
  bytes_encoded = serialize_movie(buffer, mov1);// counts for the bytes uncoded
  printf ("bytes_encoded = %d\n",(int)bytes_encoded);
  bytes_sent = send(sock, buffer, bytes_encoded, 0);// checks if the bytes sent are same as the bytes encoded
  free_movie(mov1); // gives the memory back
  if (bytes_sent != bytes_encoded) {
    fprintf(stderr, "Error calling send()\n");// if bytes sent and encoded are not same print this message
    exit(EXIT_FAILURE);
  }
  
  if ( close(sock) < 0 ) {
    fprintf(stderr, "Error calling close()\n");
    exit(EXIT_FAILURE);
  }
  
  
  return EXIT_SUCCESS;
}

  
